# Magento1_Pdfinvoice
Magento 1.x module. Extension for adding words of thanks to the buyer for purchasing in the invoice PDF.

When you install this extension in the administrative part of the Magento will create a new static CMS block - "Block for invoice PDF".
The identifier of this block will be - "block_invoice_pdf".
This block will contain the phrase: "Thank you for buying from us".
After installing this extension, this phrase appears in all the invoice PDFs.
#